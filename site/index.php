<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Главная</title> <!-- Заголовок -->
    <meta charset="utf-8"> <!-- Unicode -->

    <!-- Стили страницы -->
    <style>
      /* Подключаю шрифты */
      @import 'https://fonts.googleapis.com/css?family=Open+Sans';
      
      body * { margin: 0px; padding: 0px; font-family: 'Open Sans', sans-serif; }
      
      html, body {
          margin: 0px;   /* Внешний отступ */
          padding: 20px; /* Внутренний отступ */
          background-color: #FAFAFA; /* Цвет фона страницы */
      }
      
      .page__container {
          width: 1280px;
          margin: 0 auto;
          padding-bottom: 20px;
          background-color: #FFFFFF;
          position: relative;
      }
      
      .page__container .header {
          width: 100%;
          height: 500px;
          background: url("/img/index/header_bg.jpg") no-repeat;
      }
      
      .page__container .header .menu {
          width: 100%;
          height: 70px;
      }
      
      .page__container .header .menu ul {
          list-style-type: none;
          margin: 0;
          padding: 0;
          overflow: hidden;
      }
      
      .page__container .header .menu li {
          float: right;
      }
      
      .page__container .header .menu li a, li img {
          display: block;
          text-align: center;
          padding: 20px 20px;
          text-decoration: none;
          font-weight: 400;
          font-size: 12px;
          color: #FFFBF4;
          text-transform: uppercase;
      }
      
      .page__container .header .menu li a {
          padding-top: 30px;
      }
      
      
      .page__container .header .menu li a:hover {
          text-decoration: underline;
      }
      
      .page__container .articles {
          width: 50%;
          height: auto;
          margin: 50px auto;
      }
      
      /* Стили Статьи */
      .page__container .articles .article {
          width: 100%;
          height: 200px;
          margin-top: 50px;
      }
      
      .page__container .articles .article * {}
      
      .page__container .articles .article .time {
          width: 100%;
          font-size: 12px;
          text-align: center;
          font-style: italic;
      }
      .page__container .articles .article .time_divider {
          width: 10%;
          height: 1px;
          background-color: #333;
          margin: 20px auto 20px auto;
      }
      
      .page__container .articles .article .title {
          width: 100%;
          font-size: 16px;
          text-align: center;
          font-style: italic;
          font-weight: bold;
          margin-bottom: 10px;
      }
      
      .page__container .articles .article .content {
          width: 100%;
          font-size: 14px;
          text-align: center;
          font-style: normal;
          font-weight: normal;
      }
      
      .page__container .articles .article .content a.full {
          color: #000;
          text-decoration: underline;
      }
      
    </style>
  </head>
  <body>
    <div class="page__container">
      <div class="header">
        <div class="menu">
          <ul>
              <?php
                if(!isset($_SESSION['user_id'])) {
                    echo '<li><a href="/signin">Вход</a></li>
                          <li><a href="/signup">Регистрация</a></li>';
                } else if($_SESSION['user_id'] == 1) {
                    echo '<li><a href="/method/signout.php">Выход</a></li>
                          <li><a href="/cpanel">Управление</a></li>
                          <li><a href="/categories">Категории</a></li>
                          <li><a href="/articles">Статьи</a></li>';
                } else {
                    echo '<li><a href="/method/signout.php">Выход</a></li>
                          <li><a href="/categories">Категории</a></li>
                          <li><a href="/articles">Статьи</a></li>';
                }
              ?>
            <li><a href="/">ГЛАВНАЯ</a></li>
              <?php
                  if(isset($_SESSION['user_id'])) {
                      echo '<li style="float:left"><a href="/">@' . $_SESSION['login'] . '</a></li>';
                  }
              ?>
          </ul>
        </div>
        <h1 style="color: #FFF;width: 100%; text-align:center;margin-top:130px;font-size: 52px;font-weight: bold;">Главная</h1>
      </div>
      <div class="articles">
          <?php
              require('method/config.php');

              $db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
              if ($db->connect_error) {
                  printf("Не удалось соедениться с базой данных: %s\n", $db->connect_error);
                  exit();
              }

              $db->query("SET NAMES utf8");

              $articles = $db->query(
                  "SELECT articles.id, articles.title, articles.summary, articles.ctime 
                  FROM articles JOIN categories ON articles.category_id=categories.id ORDER BY articles.id DESC LIMIT 5");
              if(!$articles) {
                  echo '<p>Пока нет ни одной статьи.</p>';
              } else {
                  while ($a = $articles->fetch_assoc()) {
                      echo '<div class="article">
                              <p class="time">Добавлено '.$a['ctime'].'</p>
                              <div class="time_divider"></div>
                              <p class="title">'.$a['title'].'</p>
                              <p class="content">'.$a['summary'].'
                              <a href="/articles?id='.$a['id'].'" class="full">Читать далее 	&gt;&gt;</a>
                              </p>
                            </div>';
                  }
              }
          ?>
          
      </div>
    </div>
  </body>
</html>
