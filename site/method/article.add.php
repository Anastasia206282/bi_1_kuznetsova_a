<?php

require('config.php');

$db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
if ($db->connect_error) {
    printf("<b>Не удалось соедениться с базой данных: %s</b>", $db->connect_error);
    exit();
}

$db->query("SET NAMES utf8");

$errors = 0;
$comeback = '<a href="/cpanel">&lt;&lt; Управление</a>';
// Название
if(!preg_match("/^.{3,40}$/", $_POST['title'])) {
    echo '<b>Длинна названия должна быть от 5 до 40 символов</b><br>';
    $errors += 1;
}

// Описание
$desc_len = mb_strlen($_POST['summary']);
if($desc_len < 15 || $desc_len > 400) {
    echo '<b>Длинна краткого описания должна быть от 15 до 400 символов</b><br>';
    $errors += 1;
}

// Описание
$desc_len = mb_strlen($_POST['content']);
if($desc_len < 15 || $desc_len > 5000) {
    echo '<b>Длинна статьи должна быть от 100 до 5000 символов</b><br>';
    $errors += 1;
}

if($errors != 0) {
    echo $comeback;
    exit();
}
$category_id = intval($_POST['category_id']);
if($db->query(
    "INSERT INTO articles (title, summary, content, category_id)
      VALUES ('".htmlspecialchars($_POST['title'])."', '".htmlspecialchars($_POST['summary'])."', '".htmlspecialchars($_POST['content'])."', $category_id)"
)) {
    echo '<b>Статья успешно создана.</b><br>'.$comeback;
}
