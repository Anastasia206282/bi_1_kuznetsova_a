<?php

require('config.php');

$db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
if ($db->connect_error) {
    printf("<b>Не удалось соедениться с базой данных: %s</b>", $db->connect_error);
    exit();
}

$db->query("SET NAMES utf8");

$errors = 0;
$comeback = '<a href="/cpanel">&lt;&lt; Управление</a>';
// Название
if(!preg_match("/^.{3,20}$/", $_POST['name'])) {
    echo '<b>Длинна названия должна быть от 5 до 20 символов</b><br>';
    $errors += 1;
}

// Описание
$desc_len = mb_strlen($_POST['description']);
if($desc_len < 15 || $desc_len > 100) {
    echo '<b>Длинна описания должна быть от 15 до 100 символов</b><br>';
    $errors += 1;
}

if($errors != 0) {
    echo $comeback;
    exit();
}

if($db->query("INSERT INTO categories (`name`, description) VALUES ('".htmlspecialchars($_POST['name'])."', '".htmlspecialchars($_POST['description'])."')")) {
    echo '<b>Категория успешно создана.</b><br>'.$comeback;
}

