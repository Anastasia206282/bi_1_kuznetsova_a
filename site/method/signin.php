<?php

if(isset($_SESSION['user_id'])) header('Location: /');

require('config.php');

$db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
if ($db->connect_error) {
    printf("Не удалось соедениться с базой данных: %s\n", $db->connect_error);
    exit();
}

$errors = 0;
$comeback = '<a href="/signin">&lt;&lt; Войти.</a>';

$_POST['email'] = trim($_POST['email']);
$_POST['password'] = trim($_POST['password']);

// >> Проверяем данные пользователя
    // Email
    if(!preg_match("/^([a-zA-Z0-9]{3,36}@[0-9a-zA-Z]{4,10}.[a-z]{2,4}){0,50}$/", $_POST['email'])) {
        echo '<b>Введен недопустимый Email</b>. До 50-ти сиволов<br>';
        $errors += 1;
    }

    // Пароль
    if(!preg_match("/^[a-zA-Z0-9]{6,20}$/", $_POST['password'])) {
        echo '<b>Введен слишком легкий пароль.</b> Пароль должен состоять из символов a-zA-Z0-9, от 6-ти то 20-ти символов.<br>';
        $errors += 1;
    }

    if($errors != 0) {
        echo $comeback;
        exit();
    }
// <<

$db->query("SET NAMES utf8");

if(!($user = $db->query("SELECT id, login, avatar FROM users WHERE hpassword='".sha1($_POST['password'])."' AND email='{$_POST['email']}'"))) {
    echo '<b>Внутренняя ошибка сервера.</b>';
    exit();
}

if(($user = $user->fetch_assoc()) == NULL) {
    echo '<b>Либо вы ввели не верные данные, либо пользователя с таким Email не существует.</b><br>';
    echo $comeback;
    exit();
}

session_start();
$_SESSION['user_id'] = intval($user['id']);
$_SESSION['login'] = $user['login'];
$_SESSION['avatar'] = $user['avatar'];

header('Location: /');
