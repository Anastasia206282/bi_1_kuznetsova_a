<?php
session_start();

require('config.php');

$db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
if ($db->connect_error) {
    printf("<b>Не удалось соедениться с базой данных: %s</b>", $db->connect_error);
    exit();
}

$db->query("SET NAMES utf8");
$comeback = '<a href="/articles?id=\'.$_GET[\'id\'].\'">&lt;&lt; Назад к статье</a>';

if(!isset($_SESSION['user_id']) || $_SESSION['user_id'] != 1) {
    echo '<b>Удалять статьи могут только администраторы.</b><br>'.$comeback;
}

$a_id = intval($_GET['id']);
if($db->query(
    "DELETE FROM articles WHERE id = $a_id"
)) {
    echo '<b>Статья успешно удалена.</b><br>'.$comeback;
}
