<?php
require('config.php');

$db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
if ($db->connect_error) {
    printf("Не удалось соедениться с базой данных: %s\n", $db->connect_error);
    exit();
}

$errors = 0;
$comeback = '<a href="/signup">&lt;&lt; Назад к регистрации.</a>';

$_POST['login'] = trim($_POST['login']);
$_POST['name'] = trim($_POST['name']);
$_POST['email'] = trim($_POST['email']);
$_POST['password'] = trim($_POST['password']);

// >> Проверяем данные пользователя
    // Логин
    if(!preg_match("/^[a-zA-Z0-9]{3,20}$/", $_POST['login'])) {
        echo '<b>Введен недопустимый Логин</b>. Доступны символы a-zA-Z0-9 не менее 3-х и не более 20-ти символов.<br>';
        $errors += 1;
}

    // Имя
    if(!preg_match("/^[a-zA-Z0-9а-яА-Я]{3,20}$/u", $_POST['name'])) {
        echo '<b>Введено недопустимое Имя</b>. Доступны символы a-zA-Z0-9а-яА-Я не менее 3-х и не более 20-ти символов.<br>';
        $errors += 1;
    }

    // Email
    if(!preg_match("/^([a-zA-Z0-9]{3,36}@[0-9a-zA-Z]{4,10}.[a-z]{2,4}){0,50}$/", $_POST['email'])) {
        echo '<b>Введен недопустимый Email</b>. До 50-ти сиволов<br>';
        $errors += 1;
    }

    // Пароль
    if(!preg_match("/^[a-zA-Z0-9]{6,20}$/", $_POST['password'])) {
        echo '<b>Введен слишком легкий пароль.</b> Пароль должен состоять из символов a-zA-Z0-9, от 6-ти то 20-ти символов.<br>';
        $errors += 1;
    }

    // Аватар
    if($_FILES['avatar']['size'] != 0) {
        $fname = md5($_FILES['avatar']['name'].time()).'.'.explode('/',$_FILES['avatar']['type']);
        $uploadfile = "data/".$fname;
        move_uploaded_file($_FILES['avatar']['name'], $uploadfile);
    } else {
        $fname = '';
    }

    if($errors != 0) {
        echo $comeback;
        exit();
    }
// <<

$db->query("SET NAMES utf8");

if(!($user = $db->query("SELECT id FROM users WHERE email='{$_POST['email']}'"))) {
    echo '<b>Внутренняя ошибка сервера.</b>';
    exit();
}

if(($user = $user->fetch_assoc()) != NULL) {
    echo '<b>Пользователь с таким Email уже существует.</b><br>';
    echo $comeback;
    exit();
}

$hash_password = sha1($_POST['password']);
if($db->query(
    "INSERT INTO users (`name`, login, hpassword, email, avatar) VALUES ('{$_POST['name']}', '{$_POST['login']}', '$hash_password', '{$_POST['email']}', '$fname')"
)) {
    echo '<b>Вы успешно зарегистрированны.</b><br><a href="/signin">Войти &gt;&gt;</a>';
}

