<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Управление</title> <!-- Заголовок -->
    <meta charset="utf-8"> <!-- Unicode -->

    <!-- Стили страницы -->
    <style>
        /* Подключаю шрифты */
        @import 'https://fonts.googleapis.com/css?family=Open+Sans';

        body * { margin: 0px; padding: 0px; font-family: 'Open Sans', sans-serif; }

        html, body {
            margin: 0px;   /* Внешний отступ */
            padding: 20px; /* Внутренний отступ */
            background-color: #FAFAFA; /* Цвет фона страницы */
        }

        .page__container {
            width: 1280px;
            min-height: 1000px;
            margin: 0 auto;
            padding-bottom: 20px;
            background-color: #FFFFFF;
            position: relative;
        }

        .page__container .header {
            width: 100%;
            height: 500px;
            background: url("/img/index/header_bg.jpg") no-repeat;
        }

        .page__container .header .menu {
            width: 100%;
            height: 70px;
        }

        .page__container .header .menu ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        .page__container .header .menu li {
            float: right;
        }

        .page__container .header .menu li a, li img {
            display: block;
            text-align: center;
            padding: 20px 20px;
            text-decoration: none;
            font-weight: 400;
            font-size: 12px;
            color: #FFFBF4;
            text-transform: uppercase;
        }

        .page__container .header .menu li a {
            padding-top: 30px;
        }


        .page__container .header .menu li a:hover {
            text-decoration: underline;
        }

        .page__container .editor {
            width: 50%;
            height: auto;
            margin: 50px auto;
        }

        .page__container .editor #category_add input[type=text],
        .page__container .editor #category_add textarea {
            font-size: 15px;
            width: 100%;
            height: 25px;
            padding: 7px;
            margin-top: 10px;
        }

        .page__container .editor #category_add select {
            font-size: 15px;
            width: 300px;
            padding: 7px;
            margin-top: 10px;
        }

        .page__container .editor #category_add textarea{ min-height: 150px; }
        .page__container .editor #category_add input[type=submit] {
            width: 140px;
            height: 35px;
            border: none;
            border-radius: 3px;
            background-color: #5E788F;
            color: #FFF;
            font-size: 14px;
            margin-top: 15px;
            cursor: pointer;
        }

    </style>
</head>
<body>
<div class="page__container">
    <div class="header">
        <div class="menu">
            <ul>
                <?php
                if(!isset($_SESSION['user_id'])) {
                    echo '<li><a href="/signin">Вход</a></li>
                          <li><a href="/signup">Регистрация</a></li>';
                } else if($_SESSION['user_id'] == 1) {
                    echo '<li><a href="/method/signout.php">Выход</a></li>
                          <li><a href="/cpanel">Управление</a></li>
                          <li><a href="/categories">Категории</a></li>
                          <li><a href="/articles">Статьи</a></li>';
                } else {
                    echo '<li><a href="/method/signout.php">Выход</a></li>
                          <li><a href="/categories">Категории</a></li>
                          <li><a href="/articles">Статьи</a></li>';
                }
                ?>
                <li><a href="/">ГЛАВНАЯ</a></li>
                <?php
                    if(isset($_SESSION['user_id'])) {
                        echo '<li style="float:left"><a href="/">@' . $_SESSION['login'] . '</a></li>';
                    }
                ?>
            </ul>
        </div>
        <h1 style="color: #FFF;width: 100%; text-align:center;margin-top:130px;font-size: 52px;font-weight: bold;">Управление</h1>
    </div>
    <div class="editor">
        <h4>Добавить категорию</h4>
        <form method="POST" action="/method/category.add.php" id="category_add">
            <input type="text" name="name" placeholder="Название">
            <textarea name="description" rows="10" placeholder="Описание"></textarea>
            <input type="submit" value="Создать">
        </form>
        <h4 style="margin-top: 50px;">Добавить статью</h4>
        <?php

            require('../method/config.php');
    
            $db = new mysqli($DATABASE['host'], $DATABASE['user'], $DATABASE['password'], $DATABASE['database']);
            if ($db->connect_error) {
                printf("Не удалось соедениться с базой данных: %s\n", $db->connect_error);
                exit();
            }
    
            $db->query("SET NAMES utf8");
    
            $categories = $db->query("SELECT id,name FROM categories");
            if(!$categories) {
                echo '<p>Для написания статьи, нужно создать хотя бы одну категорию.</p>';
            } else {
                echo '<form method="POST" action="/method/article.add.php" id="category_add">
                <input type="text" name="title" placeholder="Название">
                <textarea name="summary" placeholder="Краткое описание"></textarea>
                Категория:
                <select name="category_id">';
                while ($c = $categories->fetch_assoc()) {
                    echo "<option value='{$c['id']}'>{$c['name']}</option>";
                }
                echo '</select>
                <textarea  style="height: 500px;" name="content" placeholder="Текст статьи"></textarea>
                <input type="submit" value="Создать">
            </form>';
            }

        ?>
    </div>
</div>
</body>
</html>
